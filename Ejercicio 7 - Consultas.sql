﻿USE m1u2h7;

/* 
  Ejercicio 1 
  Crear una vista que nos permita ver los productos 
  cuyo precio es mayor que 100. 
  Mostrar los campos CODIGO ARTICULO, SECCION Y PRECIO. Guardar la vista como CONSULTA1. 
*/
CREATE OR REPLACE VIEW consulta1 AS
  SELECT p.`CÓDIGO ARTÍCULO`,p.SECCIÓN,p.PRECIO 
    FROM 
      productos p
    WHERE
      p.PRECIO>100;

/* 
  Ejercicio 2 
  La vista anterior seria la siguiente. 
  Modificarla para ordenar por precio de forma descendente.  
*/
CREATE OR REPLACE VIEW consulta1 AS
  SELECT p.`CÓDIGO ARTÍCULO`,p.SECCIÓN,p.PRECIO 
    FROM 
      productos p
    WHERE
      p.PRECIO>100
    ORDER BY
      p.PRECIO DESC;

/* 
  Ejercicio 3 
  Crear una vista que utilice como tabla la vista CONSULTA1 y que nos muestre
  todos productos cuya sección es DEPORTES. Mostrar los campos CODIGO ARTICULO, SECCION Y PRECIO.
  Guardar la vista como CONSULTA2.   
*/
CREATE OR REPLACE VIEW consulta2 AS
  SELECT 
    c.`CÓDIGO ARTÍCULO`,c.SECCIÓN,c.PRECIO  
    FROM 
      consulta1 c 
    WHERE 
      c.SECCIÓN='DEPORTES';

/* Ejercicio 4  - solo muestra la solucion de consulta2 */

/* 
  Ejercicio 5 
  Insertar el siguiente registro en la tabla productos pero a través
  de la vista CONSULTA1. 
   
  a. Código articulo: AR90 
  b. Sección: Novedades 
  c. Precio: 5;  
*/
INSERT INTO consulta1 (`CÓDIGO ARTÍCULO`,SECCIÓN,PRECIO) 
  VALUES ('AR90','Novedades',5);

SELECT * FROM productos p WHERE p.`CÓDIGO ARTÍCULO`='AR90'; -- El registro se ha insertado
/* 
  Ejercicio 6
  Modificar la vista CONSULTA1 y activar el check en la vista en modo local.
*/
CREATE OR REPLACE VIEW consulta1 AS
  SELECT p.`CÓDIGO ARTÍCULO`,p.SECCIÓN,p.PRECIO 
    FROM 
      productos p
    WHERE
      p.PRECIO>100
    ORDER BY
      p.PRECIO DESC
  WITH LOCAL CHECK OPTION;

/* 
  Ejercicio 7
  Insertar el siguiente registro en la tabla productos pero a través de la vista CONSULTA1.
  
  a. Código articulo: AR91 
  b. Sección: Novedades 
  c. Precio: 5; 
  
  ¿Qué es lo que ocurre?. Modificar el precio a 110. 
*/
INSERT INTO consulta1 (`CÓDIGO ARTÍCULO`,SECCIÓN,PRECIO)
  VALUE ('AR91','Novedades',5); 
-- No se puede insertar en el registro por que no cumple la condicion establecida 'WITH LOCAL CHECK OPTION'
SELECT * FROM productos p WHERE p.`CÓDIGO ARTÍCULO`='AR91';
-- Modificamos el precio a 110
INSERT INTO consulta1 (`CÓDIGO ARTÍCULO`,SECCIÓN,PRECIO)
  VALUE ('AR91','Novedades',110); 

/* 
  Ejercicio 8
  Insertar el siguiente registro en la tabla productos pero a través de la vista CONSULTA2.
    a. Código articulo: AR92 
    b. Sección: Novedades 
    c. Precio: 5;   
*/
INSERT INTO consulta2(`CÓDIGO ARTÍCULO`,SECCIÓN,PRECIO)
  VALUE ('AR92','Novedades',5); -- Se crea el registro

/*
  Ejercicio 9
  Modificar la vista CONSULTA2 y activar el check
  en la vista en modo local.  
*/
CREATE OR REPLACE VIEW consulta2 AS
   SELECT 
    c.`CÓDIGO ARTÍCULO`,c.SECCIÓN,c.PRECIO  
   FROM 
      consulta1 c
    WHERE 
      c.SECCIÓN='DEPORTES'
  WITH LOCAL CHECK OPTION;


/*
  Ejercicio 10
  Insertar el siguiente registro en la tabla productos 
  pero a través de la vista CONSULTA2. 
  
  a. Código articulo: AR93 
  b. Sección: Novedades 
  c. Precio: 5; 

  ¿Qué es lo que ocurre?. Modificar la sección a DEPORTES
*/
INSERT INTO consulta2 (`CÓDIGO ARTÍCULO`,SECCIÓN,PRECIO)
  VALUE('AR93','Novedades',5); -- No entra por el with local check option 

INSERT INTO consulta2 (`CÓDIGO ARTÍCULO`,SECCIÓN,PRECIO)
  VALUE('AR93','DEPORTES',5); -- Si entra en la tabla productos

SELECT * FROM productos p WHERE p.`CÓDIGO ARTÍCULO`='AR93';

/*
  Ejercicio 11
  Modificar la vista CONSULTA2 y activar el check en la vista en modo CASCADA.   
*/
CREATE OR REPLACE VIEW consulta2 AS
   SELECT 
    c.`CÓDIGO ARTÍCULO`,c.SECCIÓN,c.PRECIO  
   FROM 
      consulta1 c
    WHERE 
      c.SECCIÓN='DEPORTES'
  WITH CASCADED CHECK OPTION;


/*
  Ejercicio 12
  Insertar el siguiente registro en la tabla productos pero a través 
  de la vista CONSULTA2. 
  
  d. Código articulo: AR94 
  e. Sección: DEPORTES 
  f. Precio: 5
  
  ¿Qué es lo que ocurre?. Modificar el precio a 200 
*/
INSERT INTO consulta2 (`CÓDIGO ARTÍCULO`,SECCIÓN,PRECIO)
  VALUE('AR94','DEPORTES',5); /* No entra por que al colocar el check option como cascade 
                                 esta restringiendo el check option de consulta1 */

INSERT INTO consulta2 (`CÓDIGO ARTÍCULO`,SECCIÓN,PRECIO)
  VALUE('AR94','DEPORTES',200); /* Si entra por cumple con las dos condiciones s/